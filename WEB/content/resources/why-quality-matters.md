+++
title = "Why Quality Matters?"
description = "Why should novice programmers care about software quality and quality improvement?"
date = "2017-08-02T12:35:41-04:00"
draft = false
toc = true
bref="The importance"
+++



The block-based programming community, including students and CS educators, should treat the issue of software quality seriously for multiple reasons.


##### Software quality impacts the functioning of modern society

Quality is essential to the everyday functioning of modern society on which our very lives depend. As Juran et al. put it in their Quality Handbook <cite>(Juran1999Quality)</cite> <blockquote>the importance of quality has continued to grow rapidly. To some extent, that growth is due in part to the continuing growth in complexity of products and systems, society's growing dependence on them, and, thus, society's growing dependence on those `quality dikes'.</blockquote> Modern society critically depends on software, which is part and parcel of an ever-growing number of goods and services. Despite its wide utilization, software is known to suffer from one of the highest failure rates across all engineering artifacts due to its poor quality <cite>(Jones2011economics)</cite>.
The quality of any product is almost entirely determined by its makers' attitude toward this issue. In that light, as a society, if we are to drastically improve software quality, we need to accordingly condition the mindset of software developers in regards to this issue. Computing education has a huge role to play in this endeavor. To that end, we advocate a radical notion of <em>promoting the culture of quality from the ground up</em>. That is, we propose that software quality be taught alongside the very fundamentals of computing, and block-based programming is at the focal point of this initiative.

##### Code quality affects learning effectiveness

Poor code quality can negatively impact the very foundations of the learning process. Poor software quality hinders the computing learner's ability to read and understand code, the medium of communication in computing. As Martin Fowler put it "Any fool can write code that a computer can understand. Good programmers write code that humans can understand" <cite>Fowler1999Refactoring</cite>. The results of a controlled experiment by Hermans et al. <cite>(Hermans2016Do)</cite> show that students find poor code quality programs hard to understand and modify. In our recent work, we also encounter the negative impact that poor software quality can have on the willingness of students to reuse and modify existing projects <cite>(Techapalokul2017Understanding)</cite>. In particular, our results suggest that poor code quality can render a project to appear uninviting to other novice programmers to modify, as compared with projects enjoying similar levels of popularity. Thus, poor code quality undermines code sharing, or remixing, an essential learning activity in block-based programming <cite>(Dasgupta2016Remixing)</cite>.

##### Focusing on software quality can foster the adoption of good programming practices

The habits and discipline required to achieve and maintain good software quality cannot be taught directly, but rather can be promoted and cultivated. 
Will Durant articulates this principle thusly: <blockquote>We are what we repeatedly do. Excellence then is not an act but a habit.</blockquote>
As it turns out, disciplined programming practices fail to naturally arise in parallel with increases in programming proficiency. As a recent study by Robles et al. shows,  students still copy and paste code even knowing how to avoid this harmful practice <cite>(Robles2017Software)</cite>. Our recent work shows that students continue to introduce quality problems in their programs, even as their levels programming proficiency keep increasing <cite>(Techapalokul2017Understandinga)</cite>. Thus, it is never too early to start educating students about software quality practices. Even introductory students should be encouraged to write not just working programs, but rather high-quality working programs.


---------------------------
<div class="small italic .text-right">
This article is a short excerpt from our position paper, <a class="black" href="http://research.cs.vt.edu/quality4blocks/preprints/Techapalokul2017Enhancing.pdf">"Enhancing Block-Based Programming Pedagogy to Promote the Culture of Quality From the Ground Up"</a>, which will be presented at the Blocks and Beyond 2 Workshop in Raleigh, NC, USA, Oct. 10, 2017
</div>