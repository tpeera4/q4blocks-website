+++
description = "A Catalog of 12 Code Smells in Scratch"
date = "2017-08-02T12:35:41-04:00"
draft = false
title = "Scratch Code Smells"
toc = true
bref="Study of code smells in Scratch projects"
+++

### Common Code Smells


##### Broad Variable Scope

A variable is marked as broad scope when the variable is made visible to all sprites, but is only used in one sprite. By following commonly accepted design practices, variables should be made local to the scope that uses it. 

**Motivation:**  
Proper variable scope helps improve comprehensibility as it tells to which sprite the variable belongs. Too many global variables can also be confusing for programmers trying to find the right variable in the script palette and the drop-down menus. 


##### Duplicate Code

A fragment of code is duplicated as a way to reuse existing functionality of code at multiple locations within the same Sprite. 

**Motivation:** 
Stick to DRY principle. Don’t repeat yourself

##### Long Script

A script is considered too long ( > 11 blocks) vertically.

**Motivation:**
An unreasonably long script can suggest inadequate decomposition and hinder code readability.

##### Uncommunicative Name

Non meaningful names such as “Sprite1” 

**Motivation:**
Non-meaninful names for sprites and variables make your code hard to understand.


### Other Code Smells


##### Unreachable Code

A script is considered unreachable if it is the receiver of a nonexistent message. This particular case is often caused by removing only the broadcast blocks without adjusting their corresponding receiver blocks. Note that our analysis disregards the fragment scripts not beginning with event blocks, as they are commonly used by Scratch programmers to experiment with code and to initialize persistent data.

**Motivation:**
Unreachable code can clutter your project. Unreachable scripts can be safely removed without affecting your project.


##### Unused Variable

The Scratch programming environment lets a programmer declare variables in the data palette before they can be used in the scripting area. However, the programming environment provides no support to check if the declared variables are unused and can be safely removed.

**Motivation:**
Too many unused variables can be confusing especially in block-based programming when you have to choose a variable to refer to in a drop-down menu.

##### Duplicate String

String values are considered duplicate if the same string values are used in multiple places.

**Motivation:** 
The motivation for this smell is similar to *Duplicate Code*: Don’t repeat yourself!


##### No-Op

A user event-based script that performs nothing can be removed. A common occurrence is event-handling code with no action associated with it.

**Motivation:**
It’s always good to make code readable. Remove codes that are no longer needed is one way to clean up your project. 


##### Feature Envy

A data producing script is in a different sprite from the one that uses the data the most. 

**Motivation:**
If there is a 1-to-1 relationship between such sprites, they should not be separated, having to talk to each other via a global variable. A better design would have a single sprite, with scripts communicating with each other via a local variable. 

##### Inappropriate Intimacy

A sprite excessively read the other sprite's local variables.

**Motivation:**
A sprite can check on other sprites' attributes through sensor blocks. However, excessively reading of other sprite's local variables can lead to high coupling between sprites.


##### Middle Man

A long chain of broadcast-receive can be used to pass a message from one script to another. 

**Motivation:**
Unnecessary long chain of delegation without actions can make your program overly complex and hard to understand.

##### Undefined Block

Scripts can be copied from different projects using the Scratch programming environment feature called “backpack”. The scripts with calls to a custom block without its definition will be rendered as undefined blocks, thus ceasing to contribute any useful functionality to the project. This smell is commonly introduced when custom block definitions are not copied and placed first. 

**Motivation:**
The rationale of this code smell is similar to *No-Op*.
It’s a good practice to keep your code clean. Remove blocks that are no longer needed is one way to clean up your project. 

