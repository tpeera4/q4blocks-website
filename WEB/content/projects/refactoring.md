+++
date = "2017-08-02T12:35:41-04:00"
draft = false
title = "Refactoring for Blocks"
toc = false
bref="A refactoring for Blocks"
type = "refactoring"
+++

#### Supporting Scratch programmers to improve their code quality
<p>We have enhanced the official Scratch editor with quality improvement support! </p>

<p style="text-align: center;">
<a href="/editor" class="button round outline" role="button">Try it now!</a>
</p>

#### Interested in participating in our online user study?

Thank you for your interest in trying out our developed tool.
We invite you to participate in a research study titled ***"Evaluating the Effectiveness of Novice-Friendly Suggestions for Code Quality Improvement".***

This study is being done by Dr. Eli Tilevich, Peeratham Techapalokul, and Prapti Khawas from Software Innovations Lab, Department of Computer Science, Virginia Tech.

The purpose of this research study is to better understand how to provide novice programmers with suggestions for code quality improvement.

If you agree to take part in this study, you will be asked to complete an online survey. This survey will ask about your experience using novice-friendly suggestions for code quality improvement in Scratch editor and it will take you approximately 15 minutes to complete.

You may not directly benefit from this research; however, we hope that your participation in the study may help us improve the developed tool in providing suggestions to encourage quality improvement practices among novice programmers in the context of block-based languages.

There are no known risks associated with this research study. No personal identifying information (e.g., name, email address or IP address, etc.) will be collected.

Your participation in this study is completely voluntary and you can withdraw at any time. You are free to skip any question that you choose.

If you have questions about this project or if you have a research-related problem, you may contact one of the researchers:

* Peeratham Techapalokul (tpeera4@cs.vt.edu)
* Prapti Khawas (prappk19@cs.vt.edu)
* Eli Tilevich (tilevich@cs.vt.edu, or (540) 231-3475)

If you have any questions concerning your rights as a research subject, you may contact you may contact the Virginia Tech Institutional Review Board at irb@vt.edu or (540) 231-3732.