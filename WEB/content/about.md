+++
date = "2017-08-02T12:10:12-04:00"
draft = false
title = "About"

+++

<!-- Our research vision is to promote the culture of quality from the ground up.
We focus our research effort on block-based programming pedagogy in introductory computing education. Our objective is to introduce practical approaches for teaching quality concepts and practices alongside the fundamentals of computing.
 -->

The key functions of modern society depend on software-based systems, with software quality critically affecting not only the utility of software applications but also their safety and security. Currently, the standard computer science (CS) curriculum introduces computing learners to this key concept very late or leaves it out completely. To address this problem, this project fundamentally rethinks the process and practices of teaching software quality to promote the culture of quality from the ground up. This project explores teaching software quality alongside the fundamentals of programming, by developing software and related teaching materials based on a professional software engineering practice for quality control known as "code refactoring." This educational intervention serves the national interest by providing a simple pathway for bringing proven, real-world software engineering practices to the introductory computer science curriculum, imparting the importance of following principled software engineering practices to novice programmers.

This project will build on the success and popularity of block-based programming, a highly effective pedagogical tool for introductory learners. The research team will investigate software quality in the context of block-based programming and how the proven, real-world software engineering practice of refactoring, a semantics-preserving program transformation that improves code and design quality, can be introduced to promote the culture of quality in introductory CS curriculum. This project will develop a novel educational intervention for teaching software quality concepts to introductory students, including teaching strategies and learning materials. This work will also help create a conceptual foundation of refactoring for block-based languages, concretely realized as refactoring support for block-based programming environments. Finally, the project team will systematically evaluate the developed educational intervention to determine its effectiveness in fostering student knowledge, skills, and practices, required to improve software quality.