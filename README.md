# Quality4Blocks

## Setup
The website uses hugo and kube theme https://github.com/jeblister/kube
so install hugo
https://github.com/gohugoio/hugo/releases
see https://gohugo.io/getting-started/installing/ for more details

then 
```
cd WEB/themes
git clone https://github.com/jeblister/kube.git
```
hugo new --kind docs docs/my-new-doc.md


## Disable cache in Chrome
For testing because somehow Chrome cache the content and doesn't load the recent content properly. Chrome DevTools->Network, check Disable Cache


## Test
cd WEB && hugo server -D


# Serving a static website in public folder
```sh
npm install serve # at the root directory
./node_modules/serve/bin/serve.js WEB/public/
```

Update repo with latest static files
---------------------------------------
DO NOT forget to change LOCAL to REMOTE API URL
```sh
cd WEB
hugo # on the WEB root to generate public directory for deployment
```

## Upload to http://research.cs.vt.edu/quality4blocks/
```sh
host: ap1.cs.vt.edu
user: tpeera4
pass: cs.vt pass
putty.exe -ssh username@ap1.cs.vt.edu
cd /web/research/quality4blocks
```

## Use sftp client such FileZilla (port 22)
---------------------------------

